﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using VirtualFair;
using VirEvent.Callbacks;

public class RRSSEditor : MonoBehaviour
{

    public int m_currentId=0;
    public Button m_AccessRRSSPanel;
    public Button m_CreateRRSS;
    public Button m_ExitBtn;
    public TMP_InputField m_FacebookInput;
    public TMP_InputField m_TwitterInput;
    public TMP_InputField m_InstagramInput;
    public TMP_InputField m_LinkedinInput;
    public TMP_InputField m_WhatsappInput;

    public GameObject LoadingRRSS;


    private void Start()
    {
        m_CreateRRSS.onClick.AddListener(CreateRRSS);

        m_ExitBtn.onClick.AddListener(()=>
        {
            gameObject.SetActive(false);
        });

        m_AccessRRSSPanel.GetComponent<Image>().color=Color.red;
    }


    private void CreateRRSS()
    {
        string fb=m_FacebookInput.text.Trim();
        string tw=m_TwitterInput.text.Trim();
        string ig=m_InstagramInput.text.Trim();
        string lk=m_LinkedinInput.text.Trim();
        string wpp=m_WhatsappInput.text.Trim();
        LoadingRRSS.SetActive(true);

        Callback.CreateRRSS(fb,tw,ig,lk,wpp,
        (id,sucess)=>
        {
            m_currentId=id;
            Debug.Log(sucess);
            LoadingRRSS.SetActive(false);
            gameObject.SetActive(false);
            m_AccessRRSSPanel.GetComponent<Image>().color=Color.green;
        },
        (error)=>
        {
            Debug.Log(error);
        });
    }

}
