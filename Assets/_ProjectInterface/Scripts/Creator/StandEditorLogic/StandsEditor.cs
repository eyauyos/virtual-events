﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using VirtualFair;
using TMPro;

public class StandsEditor : MonoBehaviour
{
    public StandBE currentStand;

    [Header("StandUI Info editor")]
    public TMP_InputField m_NameStand;
    public TMP_InputField m_EmailStand;
    public Button m_SaveBtn;
    public Button m_ExitBtn;

    [Header("RRSSUI")]
    public Button m_RRSSBtn;
    public RRSSEditor m_RRSSEditor;


    private void Start()
    {
        m_ExitBtn.onClick.AddListener(()=>
        {
            gameObject.SetActive(false);
        });

        m_RRSSBtn.onClick.AddListener(()=>
        {
            m_RRSSEditor.gameObject.SetActive(true);
        });
    }

    public void OpenStandConfig(StandFairUI standFairUI)
    {
        StandBE standBE= standFairUI.standBE;

        gameObject.SetActive(true);
        if(standBE.name!=string.Empty&&standBE.email!=string.Empty)
        {
            m_NameStand.text=standBE.name;
            m_EmailStand.text=standBE.email;
        }
        
        
        m_SaveBtn.onClick.AddListener(()=>
        {
            standBE.name=m_NameStand.text.Trim();
            standBE.email=m_EmailStand.text.Trim();
            standBE.id_rrss = m_RRSSEditor.m_currentId;
            m_SaveBtn.onClick.RemoveAllListeners();
            standFairUI.isReady=true;
            standFairUI.EnableStand();
            gameObject.SetActive(false);
        });
        
    }
}
