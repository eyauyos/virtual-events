using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using VirEvent.Callbacks;
using VirtualFair;
using TMPro;

public class ZoneStandsEditor : MonoBehaviour
{
    public GameObject ZonesUIContainer;
    public GameObject EditorPanelStands;
    private ZoneUI zoneUIObj;


    [Header("EditorPanelUI")]
    public FairZoneBE currentFairZoneBE = null;
    public ZoneUI currentZoneUI = null;
    public Button m_ExitEditorPanelStands;
    public Button m_SaveStands;
    public GameObject StandsContainer;
    public TextMeshProUGUI m_ZoneName;
    public List<StandFairUI> m_StandPositions = new List<StandFairUI>(22);
    public List<StandBE> currentStands;
    
    
    void OnEnable()
    {
        zoneUIObj = ZonesUIContainer.transform.GetChild(0).GetComponent<ZoneUI>();
        ShowZones();
        ResetStandsUI();
    }

    private void Start()
    {
        for(int i = 0; i < StandsContainer.transform.childCount; i++)
        {
            m_StandPositions.Add(StandsContainer.transform.GetChild(i).GetComponent<StandFairUI>());
        }

        m_ExitEditorPanelStands.onClick.AddListener(()=>
        {
            currentFairZoneBE = null;
            EditorPanelStands.SetActive(false);


            ResetStandsUI();

        });

        m_SaveStands.onClick.AddListener(() =>
        {
            SaveStands();
            
        });

        
    }
    
    private void ResetStandsUI() 
    {
        foreach(var stand in m_StandPositions)
        {
            stand.DisableStand();
            stand.ResetStand();
        }    
    }

    private void ShowZones()
    {
        foreach(var fairZoneBE in CreationController.FairZoneBEs)
        {
            GameObject temp = Instantiate(zoneUIObj.gameObject, ZonesUIContainer.transform);
            ZoneUI zoneUI = temp.GetComponent<ZoneUI>();
            zoneUI.fairZoneBE = fairZoneBE;

            zoneUI.button.onClick.AddListener(() =>
            {
                EditZoneOpen(zoneUI);
            });
            
            temp.SetActive(true);
        }

    }

    private void EditZoneOpen(ZoneUI zone)
    {
        currentZoneUI = zone;
        FairZoneBE fairZoneBE = zone.fairZoneBE;
        currentFairZoneBE = fairZoneBE;
        m_ZoneName.text = fairZoneBE.name;
        EditorPanelStands.SetActive(true);
    }

    private void SaveStands()
    {
        foreach(var stand in m_StandPositions)
        {
            stand.CreateStand(currentFairZoneBE.id);
            
        }

        currentZoneUI.DisableZone();
        currentZoneUI=null;
        EditorPanelStands.SetActive(false);
        ResetStandsUI();
    }


}