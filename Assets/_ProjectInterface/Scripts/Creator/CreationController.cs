﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using VirEvent.Callbacks;
using VirtualFair;

public class CreationController : MonoBehaviour
{
    public static CreationController Instance;
    public GameObject CreatorEventPanel;
    public GameObject CreatorZonesPanel;
    public GameObject EditorStandsFairPanel;

    public StandsEditor StandsEditor;

    public static VirtualEventBE virtualEventBE;
    

    [Header("Event UI Creator")]
    public TMP_InputField m_InputName;
    public TMP_InputField m_InputParticularName;
    public TMP_InputField m_InputInfo;
    public TMP_InputField m_Passcode;

    public TMP_Dropdown m_TypeEvents;
    public List<TMP_Dropdown.OptionData> optionEvents = new List<TMP_Dropdown.OptionData>();
    public Toggle m_WithAuditory;

    public Button SetEvent;

    public delegate void EventCreated(VirtualEventBE virtualEventBE);
    public static event EventCreated OnEventCreated;


    [Header("Edit Zones UI")]
    public Button m_AddZonesBtn;
    public Button m_SaveZones;
    public Transform ZonesUIContainer;

    [Header("Zone Editor UI")]
    public GameObject m_ZonesEditor;

    //public 
    public static List<FairZoneBE> FairZoneBEs= new List<FairZoneBE>();
    public static List<StandBE> standBEs = new List<StandBE>();

    private void Awake()
    {
        Instance=this;
    }


    public void Start()
    {
        CreateEventInitiator();

        EditZonesFairInitiator();
        OnEventCreated += EventCreatedUI;
        
    }

    private void EventCreatedUI(VirtualEventBE virtualEventBE)
    {
        CreationController.virtualEventBE = virtualEventBE;
        CreatorEventPanel.SetActive(false);
        CreatorZonesPanel.SetActive(true);
    }

    private void EditZonesFairInitiator()
    {
        m_AddZonesBtn.onClick.AddListener(AddZoneInfo);
        m_SaveZones.onClick.AddListener(SaveZonesData);
        ZoneEditor.OnZoneCreated += AddZoneToList;
    }

    private void AddZoneInfo()
    {
        m_ZonesEditor.SetActive(true);
        
    }

    private void AddZoneToList(FairZoneBE fairZoneBE)
    {
        
        ZoneUI zoneUI= ZonesUIContainer.transform.GetChild(0).GetComponent<ZoneUI>();
        zoneUI.fairZoneBE = fairZoneBE;
        FairZoneBEs.Add(zoneUI.fairZoneBE);
        GameObject temp = Instantiate(zoneUI.gameObject,ZonesUIContainer);
        temp.SetActive(true);
    }

    private void SaveZonesData()
    {
        foreach(var fairZoneBE in FairZoneBEs)
        {
            Callback.CreateZoneFair(virtualEventBE.id_fair, fairZoneBE.model_3d_type, fairZoneBE.name,
            (suc,id)=>
            {
                Debug.Log(suc);
                fairZoneBE.id = id;
            },
            (error)=>
            {
                Debug.Log(error);
            });
        }
        m_SaveZones.gameObject.SetActive(false);
        CreatorZonesPanel.SetActive(false);
        EditorStandsFairPanel.SetActive(true);
    }


    private void OnDestroy()
    {
        ZoneEditor.OnZoneCreated -= AddZoneToList;
        
    }

    #region EVENT CREATOR METHODS
    private void CreateEventInitiator()
    {
        optionEvents.Clear();
        optionEvents.Add(new TMP_Dropdown.OptionData(EventType.Fair.ToString()));
        optionEvents.Add(new TMP_Dropdown.OptionData(EventType.Gallery.ToString()));

        m_TypeEvents.options = optionEvents;
        SetEvent.onClick.AddListener(() =>
        {
            CreateEvent();
        });
    }

    

    public void CreateEvent()
    {
        

        switch ((EventType)m_TypeEvents.value)
        {
            case EventType.Fair:
                {
                    CreateFair(m_InputParticularName.text.Trim(), m_Passcode.text.Trim() ,m_InputInfo.text.Trim());
                }
                break;
            case EventType.Gallery:
                {

                }
                break;
        }
    }

    public void CreateFair(string name, string passcode,string info)
    {

        Callback.CreateFair(name.Trim(),
        (sucess, id_fair) =>
        {
            CreateEventFair(m_InputName.text.Trim(), info, passcode, id_fair);
            Debug.Log(sucess);
        },
        (error)=>
        {
            Debug.Log(error);
        });
    }

    public void CreateEventFair(string name, string info, string passcode,int id_fair)
    {
        Callback.CreateEvent(0, name, info, passcode, id_fair, 0, 0,
        (sucess, id_event) =>
        {
            Debug.Log(sucess + " : " + id_event + " | feria: " + id_fair);
            OnEventCreated?.Invoke(new VirtualEventBE(name, info, id_fair,0,0));
        },
        (error) =>
        {
            Debug.Log(error);
        });
    }
    #endregion
}
