﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using VirtualFair;
using TMPro;

public class ZoneUI : MonoBehaviour
{
    public FairZoneBE fairZoneBE;
    public Button button;
    
    [Header("UI elements")]
    public TextMeshProUGUI Name;
    public TextMeshProUGUI modelType;
    private void Awake()
    {
        button = GetComponent<Button>();
        Name.text = fairZoneBE.name;
        modelType.text = fairZoneBE.model_3d_type.ToString();
    }
    void OnEnable()
    {
        Name.text = fairZoneBE.name;
        modelType.text = fairZoneBE.model_3d_type.ToString();
    }

    public void DisableZone()
    {
        button.interactable=false;
    }

}
