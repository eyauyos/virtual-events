﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;
using VirEvent.Callbacks;
using VirtualFair;
using TMPro;

public class StandFairUI : MonoBehaviour
{
    public Button button;
    public StandBE standBE;
    public Color colorEnable;
    public Color colorDisable;
    public bool isReady;

    [SerializeField]private StandsEditor standsEditor;

    private void Awake()
    {
        standBE.position = Reverse(gameObject.name);
        button = GetComponent<Button>();
        colorDisable = GetComponent<Image>().color;
        standsEditor = CreationController.Instance.StandsEditor;
    }

    private void Start()
    {
        button.onClick.AddListener(() =>
        {
            standsEditor.OpenStandConfig(this);

        });
    }

    public void EnableStand()
    {
        GetComponent<Image>().color = colorEnable;
    }

    public void DisableStand()
    {
        GetComponent<Image>().color = colorDisable;
    }
    
    public void EditStandBE(StandBE standBE)
    {
        this.standBE=standBE;
    }


    public void CreateStand(int id_zone)
    {
        if (isReady)
        {
            Callback.CreateStand(id_zone, standBE.position, standBE.name, standBE.email,
            standBE.id_web, standBE.id_video, standBE.id_image, standBE.id_rrss, 0,
            (success, id) =>
            {
                Debug.Log(success);
            },
            (error) =>
            {
                Debug.Log(error);
            });
        }
        
    }

    public void ResetStand()
    {
        standBE = null;
        standBE = new StandBE();
        standBE.position = Reverse(gameObject.name);
    }

    public static string Reverse(string s)
    {
        char[] charArray = s.ToCharArray();
        Array.Reverse(charArray);
        return new string(charArray);
    }



}
