﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using VirtualFair;
using TMPro;

public class ZoneEditor : MonoBehaviour
{
    public FairZoneBE fairZoneBE;
    public TMP_InputField m_InputName;
    public TMP_InputField m_InputModel3DType;
    public Button m_SaveZoneBtn;
    public Button m_ExitWindowBtn;

    public delegate void ZoneCreated(FairZoneBE fairZoneBE);
    public static event ZoneCreated OnZoneCreated;
    private void Start()
    {
        m_SaveZoneBtn.onClick.AddListener(CreateZone);
        m_ExitWindowBtn.onClick.AddListener(() =>
        {
            gameObject.SetActive(false);
        });
    }

    private void CreateZone()
    {
        fairZoneBE = new FairZoneBE();
        fairZoneBE.name = m_InputName.text.Trim();
        fairZoneBE.model_3d_type = int.Parse(m_InputModel3DType.text.Trim());
        
        OnZoneCreated?.Invoke(fairZoneBE);
        m_InputName.text="";
        m_InputModel3DType.text = "";
        gameObject.SetActive(false);
    }
}
