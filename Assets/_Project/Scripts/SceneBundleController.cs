﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.AddressableAssets;
using UnityEngine.SceneManagement;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.ResourceManagement.ResourceLocations;
using UnityEngine.Events;
using VirEvent.Callbacks;

public class SceneBundleController : MonoBehaviour
{
    public static SceneBundleController Instance;

    public VirtualEventBE m_virtualEventBE;  
    public AssetLabelReference m_GalleriesLabel;
    public AssetLabelReference m_VirtualFairLabel;

    public List<IResourceLocation> galleries;
    public List<IResourceLocation> fairies;


    public delegate void EventCallback(VirtualEventBE virtualEvent);
    public static event EventCallback OnEventCallback;//cuando la data del evento ah sido obtenido de la data base

    public delegate void EventLoaded(VirtualEventBE virtualEvent);
    public static event EventLoaded OnEventLoaded; //cuando el evento ah sido cargado como escena

    public delegate void ResourceCharged();
    public static event ResourceCharged OnResourceCharged; //cuando la referencia hacia los recursos han sido obtenidos y organizados en listas





    private AsyncOperationHandle handle;
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Start()
    {

        DownloadResourceLocation((succes)=>
        {
            //foreach(var a in galleries)
            //{
            //    Debug.Log(a.PrimaryKey);
            //}

            //foreach (var a in fairies)
            //{
            //    Debug.Log(a.PrimaryKey);
            //}
        });

        OnEventCallback += LoadEvent;



    }


    public void LoadEvent(VirtualEventBE virtualEventBE)
    {
        EventType eventType = EventType.Fair;
        switch (eventType)
        {
            case EventType.Fair:
                {
                    IResourceLocation testScene = GetReferenceEventType(EventType.Fair,"0");
                    handle = Addressables.LoadSceneAsync(testScene, LoadSceneMode.Single);
                    handle.Completed += (a) =>
                    {
                        OnEventLoaded?.Invoke(virtualEventBE);
                    };
                    UtilsUI.Instance.CallLoadingBar(handle);
                }
                break;
            case EventType.Gallery:
                {
                    IResourceLocation testScene = GetReferenceEventType(EventType.Gallery, "0");
                    handle = Addressables.LoadSceneAsync(testScene, LoadSceneMode.Single);
                    handle.Completed += (a) =>
                    {
                        OnEventLoaded?.Invoke(virtualEventBE);
                    };
                    UtilsUI.Instance.CallLoadingBar(handle);
                }
                break;
        }
        
    }


    public IResourceLocation GetReferenceEventType(EventType eventType, string id) //PARA OBTENER LA REFERENCIA A UN tipo de escena de evento
    {
        switch (eventType)
        {
            case EventType.Fair:
                {
                    foreach (var location in fairies)
                    {
                        if (id == location.PrimaryKey)
                        {
                            Debug.Log(location.PrimaryKey);
                            return location;
                        }
                    }
                }
                break;
            case EventType.Gallery:
                {
                    foreach (var location in galleries)
                    {
                        if (id == location.PrimaryKey)
                        {
                            Debug.Log(location.PrimaryKey);
                            return location;
                        }
                    }
                }
                break;
        }
        
        return null;
    }

    #region DOWNLOAD RESOURCES DIRECTIONS
    private void DownloadResourceLocation(UnityAction<string> Success)
    {
        
        AsyncOperationHandle<IList<IResourceLocation>> handleVirtualFair = Addressables.LoadResourceLocationsAsync(m_VirtualFairLabel.labelString);
        handleVirtualFair.Completed+=GetResourceLocationFairies;

        handleVirtualFair.Completed += (a) =>
        {
            AsyncOperationHandle<IList<IResourceLocation>> handleGallery = Addressables.LoadResourceLocationsAsync(m_GalleriesLabel.labelString);
            handleGallery.Completed += GetResourceLocationGalleries;
            handleGallery.Completed += (b) =>
            {
                OnResourceCharged?.Invoke();
                Success?.Invoke("Recursos cargados");
                
            };
        };
        
    }

    

    private void GetResourceLocationFairies(AsyncOperationHandle<IList<IResourceLocation>> obj)
    {
        if (obj.Status == AsyncOperationStatus.Succeeded)
        {
            fairies = new List<IResourceLocation>(obj.Result);
        }

    }

    private void GetResourceLocationGalleries(AsyncOperationHandle<IList<IResourceLocation>> obj)
    {
        if (obj.Status == AsyncOperationStatus.Succeeded)
        {
            galleries = new List<IResourceLocation>(obj.Result);
        }

    }

    #endregion


    public void CallEvent(string code)
    {

        Callback.GetEventByPassCode(code, 
        (virEvent, sucess) =>
        {
            m_virtualEventBE = virEvent;
            OnEventCallback?.Invoke(virEvent);
            Debug.Log(sucess);
        },
        (error) =>
        {
            Debug.LogError(error);
        });

    }






    #region METODOS COMENTADOS
    //public void StartGallery()
    //{
    //    Addressables.LoadSceneAsync(m_VirtualFair, LoadSceneMode.Single).Completed += OnCompleteScene;

    //}

    //public void StartVirtualFair()
    //{
    //    Addressables.LoadSceneAsync(m_VirtualFair, LoadSceneMode.Single).Completed += OnCompleteScene;

    //}

    //private void OnCompleteScene(AsyncOperationHandle<SceneInstance> asyncOperationHandle)
    //{
    //    if (asyncOperationHandle.Status == AsyncOperationStatus.Succeeded)
    //    {
    //        OnEventLoaded?.Invoke();
    //        _refGalleryPositionPlayer = FindObjectOfType<EnvironmentController>()._refPlayerPosition;
    //        Instantiate(AppManager.Instance.m_PrefabPlayer, _refGalleryPositionPlayer.position, Quaternion.identity);

    //    }
    //}

    #endregion
}
