﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tool
{
    public static void OpenWebSite(string url)
    {
#if UNITY_WEBGL && !UNITY_EDITOR
        Application.ExternalEval("window.open('https://" + url + "');");
#else
        Application.OpenURL("https://" + url);
#endif
    }
}
