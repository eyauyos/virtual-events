﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BundleReferences
{
    public class ScenesReferences
    {
        public static string VIRTUAL_FAIR_1 = "vf1";
        public static string VIRTUAL_FAIR_2 = "vf2";
        public static string VIRTUAL_FAIR_3 = "vf3";

        public static string GALLERY_1 = "g1";
        public static string GALLERY_2 = "g2";

        public static string THEATER_1 = "t1";
        public static string THEATER_2 = "t2";
    }


}
