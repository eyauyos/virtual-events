﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Services
{
    public class PHP
    {
        public const string DOMAIN = "https://www.dazardstudio.com/";

        public const string GET_EVENT_BY_PASSCODE= DOMAIN+"Virtual_Events/php_services/GetEventsByPassCode.php";
        public const string GET_FAIR_BY_ID = DOMAIN+"Virtual_Events/php_services/GetFairById.php";
        public const string GET_ZONE_FAIR_BY_ID_FAIR = DOMAIN+"Virtual_Events/php_services/GetZoneFairByIdFair.php";
        public const string GET_STAND_BY_ID_ZONE_FAIR =  DOMAIN+"Virtual_Events/php_services/GetStandByIdZone.php";

        //GENERAL GETTERS SERVICES
        public const string GET_RRSS_BY_ID =  DOMAIN+"Virtual_Events/php_services/GetRrssById.php";

        public const string CREATE_EVENT = DOMAIN+"Virtual_Events/php_services/CreateEvent.php";
        public const string CREATE_FAIR = DOMAIN+"Virtual_Events/php_services/CreateFair.php";
        public const string CREATE_ZONE_FAIR = DOMAIN+"Virtual_Events/php_services/CreateZoneFair.php";
        public const string CREATE_STAND = DOMAIN+"Virtual_Events/php_services/CreateStand.php";
        public const string CREATE_RRSS = DOMAIN+"Virtual_Events/php_services/CreateRRSS.php";
        public const string CREATE_VIDEO = DOMAIN+"Virtual_Events/php_services/CreateVideo.php";
        public const string CREATE_IMAGE = DOMAIN+"Virtual_Events/php_services/CreateImage.php";
        public const string CREATE_WEB = DOMAIN+"Virtual_Events/php_services/CreateWeb.php";
        public const string CREATE_ALBUM_STAND = DOMAIN+"Virtual_Events/php_services/CreateAlbumStand.php";

        public const string DELETE_ZONE_FAIR = DOMAIN+"Virtual_Events/php_services/DeleteZoneFair.php";

        public class ERROR
        {
            public const string EVENT_NAME_REPEATED_ERROR = "Este passcode ya fue creado previamente";
            public const string EVENT_PASSCODE_REPEATED_ERROR = "Este nombre de evento ya fue creado previamente";
        }

    }



    
}
