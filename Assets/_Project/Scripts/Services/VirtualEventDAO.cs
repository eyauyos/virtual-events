﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Networking;
using Services;
using VirtualFair;
using System;

namespace VirEvent.Callbacks
{
    public class Callback
    {

        #region METODOS PARA OBTENER DATOS

        public static void GetRRSSById(int id_RRSS, UnityAction<RSSBE, string> Succesfully, UnityAction<string> Error)
        {

            WWWForm form = new WWWForm();

            form.AddField("id", id_RRSS.ToString());

            UnityWebRequest www = UnityWebRequest.Post(PHP.GET_RRSS_BY_ID, form);

            var request = www.SendWebRequest();

            request.completed += (b) =>
            {

                if (www.isHttpError || www.isNetworkError)
                {
                    Error?.Invoke("Error de consulta");
                }
                else
                {
                    if (www.downloadHandler.text.Trim().Equals("0 results"))
                    {
                        Error?.Invoke("0 results");
                    }
                    else
                    {
                        Debug.Log(www.downloadHandler.text);
                        string json = JsonHelper.FixJson(www.downloadHandler.text);
                        RSSBE[] RRSS = JsonHelper.FromJson<RSSBE>(json);

                        Succesfully?.Invoke(RRSS[0], json);
                    }


                }
            };

        }

        public static void GetStandByIdZoneFair(int id_zone, UnityAction<StandBE[], string> Succesfully, UnityAction<string> Error)
        {

            WWWForm form = new WWWForm();

            form.AddField("id_zone", id_zone.ToString());

            UnityWebRequest www = UnityWebRequest.Post(PHP.GET_STAND_BY_ID_ZONE_FAIR, form);

            var request = www.SendWebRequest();

            request.completed += (b) =>
            {

                if (www.isHttpError || www.isNetworkError)
                {
                    Error?.Invoke("Error de consulta");
                }
                else
                {
                    if (www.downloadHandler.text.Equals("0 results"))
                    {
                        Error?.Invoke("0 results");
                    }
                    else
                    {
                        Debug.Log(www.downloadHandler.text);
                        string json = JsonHelper.FixJson(www.downloadHandler.text);
                        StandBE[] fairZoneBEs = JsonHelper.FromJson<StandBE>(json);

                        Succesfully?.Invoke(fairZoneBEs, json);
                    }


                }
            };

        }

        public static void GetZoneFairByIdFair(int id_fair, UnityAction<FairZoneBE[], string> Succesfully, UnityAction<string> Error)
        {

            WWWForm form = new WWWForm();

            form.AddField("id_fair", id_fair.ToString());

            UnityWebRequest www = UnityWebRequest.Post(PHP.GET_ZONE_FAIR_BY_ID_FAIR, form);

            var request = www.SendWebRequest();

            request.completed += (b) =>
            {

                if (www.isHttpError || www.isNetworkError)
                {
                    Error?.Invoke("Error de consulta");
                }
                else
                {
                    if (www.downloadHandler.text.Equals("0 results"))
                    {
                        Error?.Invoke("0 results");
                    }
                    else
                    {
                        Debug.Log(www.downloadHandler.text);
                        string json = JsonHelper.FixJson(www.downloadHandler.text);
                        FairZoneBE[] fairZoneBEs = JsonHelper.FromJson<FairZoneBE>(json);

                        Succesfully?.Invoke(fairZoneBEs, json);
                    }


                }
            };

        }

        public static void GetFairById(int id, UnityAction<FairBE, string> Succesfully, UnityAction<string> Error)
        {

            WWWForm form = new WWWForm();

            form.AddField("id", id);

            UnityWebRequest www = UnityWebRequest.Post(PHP.GET_FAIR_BY_ID, form);

            var request = www.SendWebRequest();

            request.completed += (b) =>
            {

                if (www.isHttpError || www.isNetworkError)
                {
                    Error?.Invoke("Error de consulta");
                }
                else
                {
                    if(www.downloadHandler.text=="0 results")
                    {
                        Error?.Invoke("0 results");
                    }
                    else
                    {
                        string json = JsonHelper.FixJson(www.downloadHandler.text);
                        FairBE[] virtualEventBE = JsonHelper.FromJson<FairBE>(json);

                        Succesfully?.Invoke(virtualEventBE[0], json);
                    }
                    

                }
            };

        }

        public static void GetEventByPassCode(string passcode, UnityAction<VirtualEventBE, string> Succesfully, UnityAction<string> Error)
        {
            WWWForm form = new WWWForm();

            form.AddField("passcode", passcode);

            UnityWebRequest www = UnityWebRequest.Post(PHP.GET_EVENT_BY_PASSCODE, form);

            var request = www.SendWebRequest();

            request.completed += (b) =>
            {

                if (www.isHttpError || www.isNetworkError)
                {
                    Error?.Invoke("Error de consulta");
                }
                else
                {
                    if(www.downloadHandler.text.Trim().Equals("0 results"))
                    {
                        Error?.Invoke("0 results");
                    }
                    else
                    {
                        Debug.Log(www.downloadHandler.text);
                        string json = JsonHelper.FixJson(www.downloadHandler.text);
                        VirtualEventBE[] virtualEventBE = JsonHelper.FromJson<VirtualEventBE>(json);

                        Succesfully?.Invoke(virtualEventBE[0], json);
                    }
                    

                }
            };



        }

        #endregion

        #region METODOS PARA CREAR
        public static void CreateEvent(int id_user, string name, string info, string passcode, int id_fair, int id_theater, int id_gallery, UnityAction<string,int> Succesfully, UnityAction<string> Error) {
            WWWForm form = new WWWForm();

            form.AddField("id_user", id_user);
            form.AddField("name", name);
            form.AddField("info", info);
            form.AddField("passcode", passcode);
            form.AddField("id_fair", id_fair);
            form.AddField("id_theater", id_theater);
            form.AddField("id_gallery", id_gallery);

            UnityWebRequest www = UnityWebRequest.Post(PHP.CREATE_EVENT, form);

            int id_event = -3;
            
            var request = www.SendWebRequest();

            request.completed += (b) => {
                if (www.isHttpError || www.isNetworkError)
                {
                    Error?.Invoke("Error de consulta");
                    id_event = -2;

                }
                else
                {
                    int id;
                    
                    bool success = Int32.TryParse(www.downloadHandler.text, out id);
                    if(success) {
                        id_event = id;
                        Succesfully?.Invoke("Evento creado correctamente", id_event);
                    } else {
                        id_event = -1;

                        switch (www.downloadHandler.text)
                        {
                            case PHP.ERROR.EVENT_NAME_REPEATED_ERROR:
                                {
                                    Error?.Invoke(PHP.ERROR.EVENT_NAME_REPEATED_ERROR);
                                }
                                break;

                            case PHP.ERROR.EVENT_PASSCODE_REPEATED_ERROR:
                                {
                                    Error?.Invoke(PHP.ERROR.EVENT_PASSCODE_REPEATED_ERROR);
                                }
                                break;
                        }
                        
                    }
                    //Debug.Log("id event: " + www.downloadHandler.text);
                    
                }
            };
        }

        public static void CreateFair(string name, UnityAction<string,int> Succesfully, UnityAction<string> Error) {
            WWWForm form = new WWWForm();
            form.AddField("name", name);

            UnityWebRequest www = UnityWebRequest.Post(PHP.CREATE_FAIR, form);
            
            var request = www.SendWebRequest();

            int id_fair = -3;  

            request.completed += (b) => {
                if (www.isHttpError || www.isNetworkError)
                {
                    Error?.Invoke("Error de consulta");
                    id_fair = -2;
                }
                else
                {
                    int id;
                    bool success = Int32.TryParse(www.downloadHandler.text, out id);
                    if(success) {
                        id_fair = id;
                        Succesfully?.Invoke("Feria creada correctamente", id_fair);
                    } else {
                        id_fair = -1;
                        Error?.Invoke("Error al crear feria");
                    }
                    Debug.Log("id_fair: " + www.downloadHandler.text);

                    
                }
            };
        }

        public static void CreateZoneFair(int id_fair, int model_3d_type, string name, UnityAction<string,int> Succesfully, UnityAction<string> Error) {
            WWWForm form = new WWWForm();
            form.AddField("id_fair", id_fair);
            form.AddField("model_3d_type", model_3d_type);
            form.AddField("name", name);

            UnityWebRequest www = UnityWebRequest.Post(PHP.CREATE_ZONE_FAIR, form);
            
            var request = www.SendWebRequest();

            int id_zone_fair = -3;  
            request.completed += (b) => {
                if (www.isHttpError || www.isNetworkError)
                {
                    Error?.Invoke("Error de consulta");
                    id_zone_fair = -2;
                }
                else
                {
                    int id;
                    bool success = Int32.TryParse(www.downloadHandler.text, out id);
                    
                    if(success) {
                        id_zone_fair = id;
                        Succesfully?.Invoke("Zona de feria creada correctamente", id_zone_fair);
                    } else {
                        id_zone_fair = -1;
                        Error?.Invoke("Error de creación");
                    }
                    Debug.Log("id zona feria: " + www.downloadHandler.text);
                }
            };
            //return id_zone_fair;
        }

        public static void CreateStand(int id_zone, string position, string name, string email, int id_web, 
            int id_video, int id_image, int id_rrss, int id_branding, 
            UnityAction<string,int> Succesfully, UnityAction<string> Error) 
        {
            WWWForm form = new WWWForm();
            form.AddField("id_zone", id_zone);
            form.AddField("position", position);
            form.AddField("name", name);
            form.AddField("email", email);
            form.AddField("id_web", id_web);
            form.AddField("id_video", id_video);
            form.AddField("id_main_image", id_image);
            form.AddField("id_rrss", id_rrss);
            form.AddField("id_branding", id_branding);

            UnityWebRequest www = UnityWebRequest.Post(PHP.CREATE_STAND, form);
            
            var request = www.SendWebRequest();
            
            int id_stand = -3;
            request.completed += (b) => {
                if (www.isHttpError || www.isNetworkError)
                {
                    Error?.Invoke("Error de creación de stand");
                    id_stand = -2; 
                }
                else
                {
                    
                    int id;
                    bool success = Int32.TryParse(www.downloadHandler.text, out id);
                    if(success) {
                        id_stand = id;
                        Succesfully?.Invoke("Stand creado correctamente",id_stand);
                    } else {
                        id_stand = -1;
                        Error?.Invoke("Error de creación de stand");
                    }
                    Debug.Log("id_stand: " + www.downloadHandler.text);
                }
            };
        }

        public static int CreateVideo(string url, UnityAction<string> Succesfully, UnityAction<string> Error) {
            WWWForm form = new WWWForm();
            form.AddField("url", url);

            UnityWebRequest www = UnityWebRequest.Post(PHP.CREATE_VIDEO, form);
            var request = www.SendWebRequest();
            
            int id_video = -3;

            request.completed += (b) => {
                if (www.isHttpError || www.isNetworkError)
                {
                    Error?.Invoke("Error de consulta");
                    id_video = -2; 
                }
                else
                {
                    Succesfully?.Invoke("Video creado correctamente");
                    int id;
                    bool success = Int32.TryParse(www.downloadHandler.text, out id);
                    if(success) {
                        id_video = id;
                    } else {
                        id_video = -1;
                    }
                    Debug.Log("id_video: " + www.downloadHandler.text);
                }
            };
            return id_video;
        }

        public static int CreateImage(string url, int order, UnityAction<string> Succesfully, UnityAction<string> Error) {
            WWWForm form = new WWWForm();
            form.AddField("url", url);
            form.AddField("order", order);

            
            UnityWebRequest www = UnityWebRequest.Post(PHP.CREATE_IMAGE, form);

            var request = www.SendWebRequest();
            
            int id_image = -3;

            request.completed += (b) => {
                if (www.isHttpError || www.isNetworkError)
                {
                    Error?.Invoke("Error de consulta");
                    id_image = -2; 
                }
                else
                {
                    Succesfully?.Invoke("Imagen creada correctamente");
                    int id;
                    bool success = Int32.TryParse(www.downloadHandler.text, out id);
                    if(success) {
                        id_image = id;
                    } else {
                        id_image = -1;
                    }
                    Debug.Log("id_image: " + www.downloadHandler.text);
                }
            };
            return id_image;
        }

        public static int CreateAlbumStand(int id_owner, int id_image, UnityAction<string> Succesfully, UnityAction<string> Error) {
            WWWForm form = new WWWForm();
            form.AddField("id_owner", id_owner);
            form.AddField("id_image", id_image);

            
            UnityWebRequest www = UnityWebRequest.Post(PHP.CREATE_STAND, form);

            var request = www.SendWebRequest();
            
            int id_album_stand = -3;

            request.completed += (b) => {
                if (www.isHttpError || www.isNetworkError)
                {
                    Error?.Invoke("Error de consulta");
                    id_album_stand = -2; 
                }
                else
                {
                    Succesfully?.Invoke("Imagen creada correctamente");
                    int id;
                    bool success = Int32.TryParse(www.downloadHandler.text, out id);
                    if(success) {
                        id_album_stand = id;
                    } else {
                        id_album_stand = -1;
                    }
                    Debug.Log("id_image: " + www.downloadHandler.text);
                }
            };
            return id_album_stand;
        }
        
        public static int CreateWeb(string url, UnityAction<string> Succesfully, UnityAction<string> Error) {
            WWWForm form = new WWWForm();
            form.AddField("url", url);

            UnityWebRequest www = UnityWebRequest.Post(PHP.CREATE_WEB, form);

            var request = www.SendWebRequest();
            
            int id_web = -3;

            request.completed += (b) => {
                if (www.isHttpError || www.isNetworkError)
                {
                    Error?.Invoke("Error de consulta");
                    id_web = -2; 
                }
                else
                {
                    Succesfully?.Invoke("Imagen creada correctamente");
                    int id;
                    bool success = Int32.TryParse(www.downloadHandler.text, out id);
                    if(success) {
                        id_web = id;
                    } else {
                        id_web = -1;
                    }
                    Debug.Log("id_web: " + www.downloadHandler.text);
                }
            };
            return id_web;
        }

        public static void CreateRRSS(string facebook_url, string twitter_url, string instagram_url, string linkedn_url, string whatsapp_url, UnityAction<int,string> Succesfully, UnityAction<string> Error) {
            WWWForm form = new WWWForm();
            form.AddField("facebook_url", facebook_url);
            form.AddField("twitter_url", twitter_url);
            form.AddField("instagram_url", instagram_url);
            form.AddField("linkedn_url", linkedn_url);
            form.AddField("whatsapp_url", whatsapp_url);
            
            UnityWebRequest www = UnityWebRequest.Post(PHP.CREATE_RRSS, form);
            
            var request = www.SendWebRequest();
            
            int id_RRSS = -3;

            request.completed += (b) => {
                if (www.isHttpError || www.isNetworkError)
                {
                    
                    id_RRSS = -2;
                    Error?.Invoke("Error de consulta");
                }
                else
                {
                    
                    int id;
                    bool success = Int32.TryParse(www.downloadHandler.text, out id);
                    if(success) {
                        id_RRSS = id;
                    } else {
                        id_RRSS = -1;
                    }
                    //Debug.Log("id_RRSS: " + www.downloadHandler.text);
                    Succesfully?.Invoke(id,"RRSS creada correctamente");
                }
            };
        }

        #endregion
    
        #region METODOS PARA ELIMINAR DATOS


        public static void DeleteZoneFair(string id_zone, UnityAction<string> sucess, UnityAction<string> error)
        {
            WWWForm form = new WWWForm();
            form.AddField("id",id_zone);

            UnityWebRequest www = UnityWebRequest.Post(PHP.DELETE_ZONE_FAIR,form);

            var a = www.SendWebRequest();

            a.completed += (b)=>
            {
                if(www.isHttpError||www.isNetworkError)
                {
                    Debug.Log(www.downloadHandler.text);
                }
                else
                {
                    Debug.Log(www.downloadHandler.text);
                }



            };

        }

        #endregion
    }
}






