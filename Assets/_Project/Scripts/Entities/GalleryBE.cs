﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GalleryBE
{
    public int idEvent;
    public int numberPaints;
    public GalleryType galleryType;

    public GalleryBE(int idEvent, int numberPaints, int galleryType)
    {
        this.idEvent = idEvent;
        this.numberPaints = numberPaints;
        this.galleryType = (GalleryType)galleryType;
    }

    public enum GalleryType
    {
        hexagonal,
        quadrangular
    }
}
