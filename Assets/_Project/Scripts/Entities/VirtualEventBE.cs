﻿using System;
using VirtualFair;

[Serializable]
public class VirtualEventBE
{
    public string name;
    public string info;
    public int id_fair;
    public int id_theater;
    public int id_gallery;

    public EventType eventType;

    public VirtualEventBE(string name, string info, int id_fair, int id_theater, int id_gallery)
    {
        this.name = name;
        this.info = info;
        this.id_fair = id_fair;
        this.id_theater = id_theater;
        this.id_gallery = id_gallery;

        SetEventType();
    }

    public void SetEventType()
    {
        if (id_fair == 1)
            eventType = EventType.Fair;
        else if (id_theater == 1)
            eventType = EventType.Fair;
        else if (id_gallery == 1)
            eventType = EventType.Fair;
    }
}

public enum EventType
{
    Fair,
    Gallery,
    Auditory
}
