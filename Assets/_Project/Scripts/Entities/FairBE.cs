﻿using System;
using System.Collections.Generic;
using UnityEngine;


namespace VirtualFair
{
    [Serializable]
    public class FairBE
    {
        public string id;
        public string name;

        public FairBE(string id, string name)
        {
            this.id = id;
            this.name = name;
        }
    }

    [Serializable]
    public class FairZoneBE
    {
        public int id;
        public int model_3d_type;
        public string name;

    }

    [Serializable]
    public class StandBE
    {
        public int id;
        public string position;
        public string name;
        public string email;
        public int id_web;
        public int id_video;
        public int id_image;
        public int id_rrss;
        public int id_branding;

        public StandBE()
        {
            this.id = 0;
            this.position = "";
            this.name = "default";
            this.email = "";
            this.id_web = 0;
            this.id_video = 0;
            this.id_image = 0;
            this.id_rrss = 0;
            this.id_branding = 0;
        }
        public StandBE(int id, string position, string name, string email, int id_web, int id_video, int id_image, int id_rrss, int id_branding)
        {
            this.id = id;
            this.position = position;
            this.name = name;
            this.email = email;
            this.id_web = id_web;
            this.id_video = id_video;
            this.id_image = id_image;
            this.id_rrss = id_rrss;
            this.id_branding = id_branding;
        }
    }

    [Serializable]
    public class RSSBE
    {
        public string facebook_url;
        public string twitter_url;
        public string instagram_url;
        public string linkedn_url;
        public string whatsapp_url;

        public RSSBE(string facebook_url, string twitter_url, string instagram_url, string linkedn_url, string whatsapp_url)
        {
            this.facebook_url = facebook_url;
            this.twitter_url = twitter_url;
            this.instagram_url = instagram_url;
            this.linkedn_url = linkedn_url;
            this.whatsapp_url = whatsapp_url;
        }
    }

    [Serializable]
    public class AlbumStandBE
    {
        public List<string> iamgeUrl;
        public int order;
    }

}

