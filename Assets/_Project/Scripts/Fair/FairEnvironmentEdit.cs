﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FairEnvironmentEdit : MonoBehaviour
{
    public FairData fairData;

    public List<Transform> StandsParent;

    public List<Transform> Models3D;
    public List<List<Transform>> groups = new List<List<Transform>>();


    public void GenerateScripteableData()
    {
        fairData.CleanAllData();

        foreach (var parent in StandsParent)
        {
            List<Transform> group = new List<Transform>();

            for (int i = 0; i < parent.childCount; i++)
            {
                group.Add(parent.GetChild(i));
            }

            groups.Add(group);
        }


        int order = 0;
        foreach (var standColumns in groups)
        {

            fairData.FairDataList.Add(CartesianGroup.GetCartesianGroup(order, standColumns));
            order++;
        }


        foreach (var model in Models3D)
        {
            CustomTransform modelInformation = new CustomTransform(model);
            fairData.Models3DList.Add(modelInformation);
        }
    }


}
