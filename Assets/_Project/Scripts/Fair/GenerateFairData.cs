﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(FairEnvironmentEdit))]
public class GenerateFairData : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        if (GUILayout.Button("Generate Data"))
        {
            FairEnvironmentEdit fairSceneManagement = (FairEnvironmentEdit)target;
            fairSceneManagement.GenerateScripteableData();
        }

    }
}
