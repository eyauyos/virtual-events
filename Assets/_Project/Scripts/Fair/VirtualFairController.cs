﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.AddressableAssets;
using UnityEngine.SceneManagement;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.ResourceManagement.ResourceLocations;
using UnityEngine;
using VirEvent.Callbacks;
using VirtualFair;
public class VirtualFairController : MonoBehaviour
{
    public static VirtualFairController Instance;

    public FairData m_fairData;
    public FairBE m_fairBE;
    public List<FairZoneBE> m_fairZoneBEs;

    public delegate void CompleteData();
    public static event CompleteData OnCompleteFairData;

    public delegate void CompleteZonesFairData(List<FairZoneBE> fairZoneBEs);
    public static event CompleteZonesFairData OnCompleteZonesFairData;

    //public GameObject standObject;
    //public List<GameObject> CurrentGameObjects = new List<GameObject>();


    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Start()
    {
        SceneBundleController.OnEventLoaded += (virEvent) =>
        {
            Callback.GetFairById(virEvent.id_fair, 
            (fairBE,sucess) =>
            {
                m_fairBE = fairBE;
                Debug.Log(sucess);
                OnCompleteFairData?.Invoke();
            },
            (error)=>
            {

                Debug.Log(error);

            });
        };


        OnCompleteFairData += CallZones;

    }


    public AsyncOperationHandle LoadFairZoneScene()
    {
        AsyncOperationHandle asyncOpHandle;

        IResourceLocation testScene = SceneBundleController.Instance.GetReferenceEventType(EventType.Fair, "1");
        asyncOpHandle = Addressables.LoadSceneAsync(testScene, LoadSceneMode.Single);
        UtilsUI.Instance.CallLoadingBar(asyncOpHandle);

        return asyncOpHandle;
    }

    private void CallZones()
    {

        Callback.GetZoneFairByIdFair(SceneBundleController.Instance.m_virtualEventBE.id_fair,
        (fairZoneBEs,sucess) =>
        {
            m_fairZoneBEs = new List<FairZoneBE>(fairZoneBEs);
            OnCompleteZonesFairData?.Invoke(m_fairZoneBEs);
        },
        (error) =>
        {
            Debug.Log(error);
        });


    }
}
