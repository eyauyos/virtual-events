﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using VirtualFair;

public class VirtualFairPanel : MonoBehaviour
{
    public GameObject m_VFPanel;
    public Button m_EnablePanelBtn;
    public Button m_DisablePanelBtn;
    [Header("Zones Panel")]
    public ScrollRect m_ZoneFairSV;


    private AsyncOperationHandle asyncOperation;
    private GameObject ZoneFairBtn;
    private List<Button> zoneFairBtns=new List<Button>();

    void Start()
    {
        m_ZoneFairSV.content.localPosition = Vector3.zero;
        ZoneFairBtn = m_ZoneFairSV.content.transform.GetChild(0).gameObject;

        

        m_EnablePanelBtn.onClick.AddListener(() =>
        {
            m_VFPanel.SetActive(true);
        });

        m_DisablePanelBtn.onClick.AddListener(() =>
        {
            m_VFPanel.SetActive(false);
        });

        VirtualFairController.OnCompleteZonesFairData += SetZonesFairData;
    }


    private void SetZonesFairData(List<FairZoneBE> fairZoneBEs)
    {
        foreach(var fz in fairZoneBEs)
        {
            GameObject tempFz = Instantiate(ZoneFairBtn, m_ZoneFairSV.content.transform);
            tempFz.GetComponentInChildren<TextMeshProUGUI>().text=fz.name;
            tempFz.SetActive(true);
            zoneFairBtns.Add(tempFz.GetComponent<Button>());
        }

        for(int i = 0; i < zoneFairBtns.Count; i++)
        {
            SetBtnZoneFairAction(i);
        }




    }

    private void SetBtnZoneFairAction(int index)
    {
        zoneFairBtns[index].onClick.AddListener(() =>
        {
            asyncOperation = VirtualFairController.Instance.LoadFairZoneScene();
            asyncOperation.Completed +=
            (async) =>
            {
                if (async.Status == AsyncOperationStatus.Succeeded)
                {
                    m_VFPanel.SetActive(false);

                    FairZoneController fairZoneController = FindObjectOfType<FairZoneController>();
                    fairZoneController.m_fairZoneBE = VirtualFairController.Instance.m_fairZoneBEs[index];
                    fairZoneController.LoadFairZone();
                }
                
                
            };
        });
    }

}
