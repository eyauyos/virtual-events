﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using VirtualFair;

public class FairUIController : MonoBehaviour
{
    [SerializeField] TMP_Text nameText;
    [SerializeField] TMP_Text emailText;
    [SerializeField] GameObject panel;
    [SerializeField] Button closeButton;
    [SerializeField] GameObject rrssParent;
    private int rrssIndex = 0;
    public void UpdateInfo(string name, string email, RSSBE standRSSBE)
    {
        panel.SetActive(true);
        nameText.text = name;
        emailText.text = email;
        rrssIndex = 0;
        setButtonRRSSInfo(standRSSBE.facebook_url, "Boton de Facebook");
        setButtonRRSSInfo(standRSSBE.twitter_url, "Boton de Twitter");
        setButtonRRSSInfo(standRSSBE.instagram_url, "Boton de Instagram");
        setButtonRRSSInfo(standRSSBE.linkedn_url, "Boton de Linkedn");
        setButtonRRSSInfo(standRSSBE.whatsapp_url, "Boton de Whatsapp");
    }

    private void setButtonRRSSInfo(string currentURL, string textButton)
    {
        if (currentURL != "")
        {
            rrssParent.transform.GetChild(rrssIndex).gameObject.SetActive(true);
            rrssParent.transform.GetChild(rrssIndex).GetChild(0).gameObject.GetComponent<TMP_Text>().text = textButton;
            rrssParent.transform.GetChild(rrssIndex).gameObject.GetComponent<Button>().onClick.AddListener(() => {
                Tool.OpenWebSite(currentURL);
            });
            rrssIndex++;
        }
    }
    void Start() {
        closeButton.onClick.AddListener(() => {
            panel.SetActive(false);
            foreach(Transform child in rrssParent.transform) {
                child.gameObject.SetActive(false);
            }
        });
    }
}
