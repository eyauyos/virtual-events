﻿using UnityEngine;
using UnityEngine.UI;
using VirtualFair;
using VirEvent.Callbacks;

public class StandFair : MonoBehaviour
{
    public StandBE StandBE;
    public RSSBE standRSSBE;
    [Header("StandUI")]
    public Button OpenStandBtn;
    private FairUIController fc;

    private void Awake()
    {
        fc = FindObjectOfType<FairUIController>();
    }
    void Start() {
        OpenStandBtn.onClick.AddListener(()=>fc.UpdateInfo(StandBE.name, StandBE.email, standRSSBE));
    }
}
