﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.AddressableAssets;
using UnityEngine.SceneManagement;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.ResourceManagement.ResourceLocations;
using UnityEngine.Events;
using VirEvent.Callbacks;
using VirtualFair;

public class FairZoneController : MonoBehaviour
{

    
    public FairZoneBE m_fairZoneBE;

    public AssetLabelReference m_StandsLabel;
    public AssetLabelReference m_Models3DLabel;

    public List<IResourceLocation> stands;
    public List<IResourceLocation> model3d;

   
    public delegate void ResourceCharged();
    public event ResourceCharged OnResourceCharged;

    public List<StandBE> m_standBEs;
    public GameObject standObject;

    //public GameObject m_StandsParent;

    public void LoadFairZone()
    {
        //Debug.Log("ejecución");
        //m_standBEs.Clear();

        //if(VirtualFairController.Instance.CurrentGameObjects.Count>0)
        //    DestroyCurrentGameObjects(VirtualFairController.Instance.CurrentGameObjects);


        DownloadResourceLocation((sucess) =>
        {
            Debug.Log(sucess);
        });

        OnResourceCharged += GetStandGameObject;
    }

    private void GetStandGameObject()
    {
        AsyncOperationHandle<GameObject> goHandle = Addressables.LoadAssetAsync<GameObject>(stands[0]);
        goHandle.Completed += (async) =>
        {
            if (async.Status == AsyncOperationStatus.Succeeded)
            {
                standObject = async.Result;

                Callback.GetStandByIdZoneFair(m_fairZoneBE.id, (stands, sucess) =>
                {
                    m_standBEs = new List<StandBE>(stands);
                    //StandFair[] currentStands = FindObjectsOfType<StandFair>();
                    //DestroyCurrentStands(currentStands);
                    InstanceStandsInPositions();
                },
                (error) =>
                {
                });
            }
            

            
        };

        UtilsUI.Instance.CallLoadingBar(goHandle);

    }

    private void InstanceStandsInPositions()
    {
        foreach(var stand in m_standBEs)
        {
            InstanceFromPosition(stand.position,stand);
        }

    }

    private void InstanceFromPosition(string position,StandBE standBE)
    {
        CustomTransform transform = VirtualFairController.Instance.m_fairData.GetPosition(position);

        //Debug.Log(transform.position);
        GameObject temp_stand = Instantiate(standObject, transform.position, transform.rotation);
        temp_stand.GetComponent<StandFair>().StandBE=standBE;
        //VirtualFairController.Instance.CurrentGameObjects.Add(temp_stand);
    }

    public void DestroyCurrentGameObjects(List<GameObject> gameObjects)
    {
        foreach (var stand in gameObjects)
        {
            Destroy(stand.gameObject);
        }

        gameObjects.Clear();
    }


    #region DOWNLOAD RESOURCES DIRECTIONS
    public void DownloadResourceLocation(UnityAction<string> Success)
    {

        AsyncOperationHandle<IList<IResourceLocation>> handleVirtualFair = Addressables.LoadResourceLocationsAsync(m_StandsLabel.labelString);
        handleVirtualFair.Completed += GetResourceLocationStands;

        handleVirtualFair.Completed += (a) =>
        {
            AsyncOperationHandle<IList<IResourceLocation>> handleGallery = Addressables.LoadResourceLocationsAsync(m_Models3DLabel.labelString);
            handleGallery.Completed += GetResourceLocationModels3D;
            handleGallery.Completed += (b) =>
            {
                OnResourceCharged?.Invoke();
                Success?.Invoke("Recursos de la feria han sido cargados");

            };
        };

    }

    private void GetResourceLocationStands(AsyncOperationHandle<IList<IResourceLocation>> obj)
    {
        if (obj.Status == AsyncOperationStatus.Succeeded)
        {
            stands = new List<IResourceLocation>(obj.Result);
        }
    }

    private void GetResourceLocationModels3D(AsyncOperationHandle<IList<IResourceLocation>> obj)
    {
        if (obj.Status == AsyncOperationStatus.Succeeded)
        {
            model3d = new List<IResourceLocation>(obj.Result);
        }
    }
    #endregion


    private void OnDestroy()
    {
        OnResourceCharged -= GetStandGameObject;
    }

}
