﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using BundleReferences;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.ResourceManagement.ResourceProviders;
using UnityEngine.ResourceManagement.ResourceLocations;
using System.Runtime.InteropServices;
using TMPro;

public class UtilsUI : MonoBehaviour
{
    public static UtilsUI Instance;
    public LoadingBar m_LoadingBar;
    // Start is called before the first frame update
    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }


    public void CallLoadingBar(AsyncOperationHandle handle)
    {
        m_LoadingBar.gameObject.SetActive(true);
        StartCoroutine(LoadingBarCoroutine(handle));
        handle.Completed += CompletedCharge;
    }





    #region PRIVATE METHODS

    private IEnumerator LoadingBarCoroutine(AsyncOperationHandle handle)
    {
        //handle = Addressables.LoadSceneAsync(test, LoadSceneMode.Single);
        bool charge = true;
        while (charge)
        {
            float progress = Mathf.Clamp01(handle.PercentComplete);
            float percentProgress = Mathf.Round(progress * 100);
            m_LoadingBar.Slider.value = progress;
            if (handle.PercentComplete == 1)
            {
                charge = false;
            }
            yield return null;
        }

    }


    private void CompletedCharge(AsyncOperationHandle handle)
    {
        StartCoroutine(CompletedChargeCoroutine(handle));
    }

    private IEnumerator CompletedChargeCoroutine(AsyncOperationHandle handle)
    {
        if (handle.Status == AsyncOperationStatus.Succeeded)
        {
            yield return new WaitForSeconds(1);
            m_LoadingBar.gameObject.SetActive(false);
        }
    }
    #endregion

}
