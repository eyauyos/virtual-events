﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class MainMenuUIController : MonoBehaviour
{
    public static MainMenuUIController Instance;

    public TMP_InputField m_InputCodeEvent;
    public Button m_Entry;
    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        m_Entry.onClick.AddListener(() =>
        {
            SceneBundleController.Instance.CallEvent(m_InputCodeEvent.text.Trim());
        });

    }


}
