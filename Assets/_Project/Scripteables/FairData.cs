﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "FairData", menuName = "FairData", order = 1)]
public class FairData : ScriptableObject
{
    public List<CartesianGroup> FairDataList = new List<CartesianGroup>();
    public List<CustomTransform> Models3DList = new List<CustomTransform>();


    public void CleanAllData()
    {
        FairDataList.Clear();
        Models3DList.Clear();
    }

    public CustomTransform GetPosition(string position)
    {
        CustomTransform customTransform = null;

        foreach(var cg in FairDataList)
        {
            if (cg.Name == (OrderLetter)(position[1]-65))
            {
                customTransform = cg.group[int.Parse(position[0].ToString()) - 1];
            }

        }
        Debug.Log(int.Parse(position[0].ToString()));
        Debug.Log(((OrderLetter)position[1]-65).ToString());
        return customTransform;
    }

}




[Serializable]
public class CartesianGroup
{
    public OrderLetter Name;
    public List<CustomTransform> group;


    public static CartesianGroup GetCartesianGroup(int order,List<Transform> group)
    {
        CartesianGroup cg = new CartesianGroup();

        cg.Name = (OrderLetter)order;
        cg.group = new List<CustomTransform>();
        foreach (var g in group)
        {
            cg.group.Add(new CustomTransform(g));
        }

        return cg;

    }


}

[Serializable]
public class CustomTransform
{
    public Vector3 position;
    public Quaternion rotation;
    public Vector3 scale;

    public CustomTransform(Transform transform)
    {
        position = transform.position;
        rotation = transform.rotation;
        scale = transform.lossyScale;
    }

    public Transform GetTransform()
    {
        Transform transform=null;
        transform.position = position;
        transform.rotation = rotation;
        transform.localScale = scale;

        return transform;
    }
}

public enum OrderLetter
{
    A,
    B,
    C,
    D,
    E,
    F,
    G
}

