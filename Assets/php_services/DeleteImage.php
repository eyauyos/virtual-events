<?php

header("Access-Control-Allow-Origin: *");

require_once 'app_config.php';
require_once 'util_functions.php';
require_once 'mainFunctions.php';

####################################################################################################################
# ENTRADA: id
# FUNCIONAMIENTO: Elimina una imagen y setea a 0 los id de esa imagen contenidas en otras tablas.
# SALIDA: devuelve informacion de error o caso contrario informa que fue borrado correctamente
####################################################################################################################

$id_img = checkNull($_POST["id"]);

deleteImage($conn, $id_img);

mysqli_close($conn);

?>