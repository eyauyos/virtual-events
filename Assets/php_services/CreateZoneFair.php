<?php

header("Access-Control-Allow-Origin: *");

require_once 'app_config.php';
require_once 'util_functions.php';
require_once 'mainFunctions.php';

####################################################################################################################
# ENTRADA: id_fair
# FUNCIONAMIENTO: Crea una nueva zona de feria con el id_fair ingresado
# SALIDA: devuelve informacion de error o caso contrario informa que fue insertado correctamente
####################################################################################################################

$id_fair = checkNull($_POST["id_fair"]);
$model_3d_type = checkNull($_POST["model_3d_type"]);
$name = checkNull($_POST["name"]);


# Value NULL significa no asignado, para id_fair, model_3d_type, name.

createZoneFair($conn, $id_fair, $model_3d_type, $name);

mysqli_close($conn);

?>