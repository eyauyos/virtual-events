<?php

header("Access-Control-Allow-Origin: *");

require_once 'app_config.php';
require_once 'util_functions.php';
require_once 'mainFunctions.php';

$id = checkNull($_POST["id"]);

####################################################################################################################
# ENTRADA: id : el id del stand
# FUNCIONAMIENTO: hace una consulta a la base de datos y devuelve las fotografias que estan en el album de ese stand
# SALIDA: devuelve las fotografias que estan en el album de ese stand en forma de JSON
#         el JSON contiene: id_url, order
####################################################################################################################

getAlbumPhotosByIdStand($conn, $id);

mysqli_close($conn);


?>