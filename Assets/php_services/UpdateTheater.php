<?php

header("Access-Control-Allow-Origin: *");

require_once 'app_config.php';
require_once 'util_functions.php';
require_once 'mainFunctions.php';

####################################################################################################################
# ENTRADA: name, newName
# FUNCIONAMIENTO: actualiza con los valores ingresados la auditorio con name en especifico
# SALIDA: devuelve informacion de error o caso contrario informa que fue actualizado correctamente
####################################################################################################################


$name = checkNull($_POST["name"]);
$newName = checkNull($_POST["newName"]);

# Value NULL significa no asignado, para tipo feria, galeria, teatro.

updateTheater($conn, $name, $newName);

mysqli_close($conn);


?>