<?php

header("Access-Control-Allow-Origin: *");

require_once 'app_config.php';
require_once 'util_functions.php';
require_once 'mainFunctions.php';

####################################################################################################################
# ENTRADA: id, facebook_url, twitter_url, instagram_url, linkedn_url, whatsapp_url
# FUNCIONAMIENTO: actualiza con los valores ingresados la rrss con id en especifico
# SALIDA: devuelve informacion de error o caso contrario informa que fue actualizado correctamente
####################################################################################################################

$id = checkNull($_POST["id"]);
$facebook_url = checkNull($_POST["facebook_url"]);
$twitter_url = checkNull($_POST["twitter_url"]);
$instagram_url = checkNull($_POST["instagram_url"]);
$linkedn_url = checkNull($_POST["linkedn_url"]);
$whatsapp_url = checkNull($_POST["whatsapp_url"]);

# Value NULL significa no asignado, para tipo feria, galeria, teatro.

updateRRSS($conn, $facebook_url, $twitter_url, $instagram_url, $linkedn_url, $whatsapp_url, $id);

mysqli_close($conn);


?>