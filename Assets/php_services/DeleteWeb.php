<?php

header("Access-Control-Allow-Origin: *");

require_once 'app_config.php';
require_once 'util_functions.php';
require_once 'mainFunctions.php';

####################################################################################################################
# ENTRADA: id
# FUNCIONAMIENTO: Elimina una web y setea a 0 los id de esa web contenida en otras tablas.
# SALIDA: devuelve informacion de error o caso contrario informa que fue borrado correctamente
####################################################################################################################

$id_web = checkNull($_POST["id"]);

deleteWeb($conn, $id_web);
mysqli_close($conn);

?>