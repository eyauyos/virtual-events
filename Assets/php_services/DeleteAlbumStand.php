<?php

header("Access-Control-Allow-Origin: *");

require_once 'app_config.php';
require_once 'util_functions.php';
require_once 'mainFunctions.php';

####################################################################################################################
# ENTRADA: id_stand
# FUNCIONAMIENTO: Elimina el Album Stand y todas las imagenes que estan dentro de ese Album Stand y elimina sus dependencias.
# SALIDA: devuelve informacion de error o caso contrario informa que fue borrado correctamente
####################################################################################################################

$id_stand = checkNull($_POST["id_stand"]);

deleteAlbumStand($conn, $id_stand);

mysqli_close($conn);

?>