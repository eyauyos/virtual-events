<?php

header("Access-Control-Allow-Origin: *");

require_once 'app_config.php';
require_once 'util_functions.php';
require_once 'mainFunctions.php';

####################################################################################################################
# ENTRADA: id_owner, id_image
# FUNCIONAMIENTO: toma los valores de entrada y los inserta un nuevo elemento a la tabla Album_stand
# SALIDA: devuelve informacion de error o caso contrario informa que fue insertado correctamente
####################################################################################################################


$id_owner = checkNull($_POST["id_owner"]);
$id_image = checkNull($_POST["id_image"]);

createAlbumStand($conn, $id_owner, $id_image);

mysqli_close($conn);

?>