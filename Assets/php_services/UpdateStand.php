<?php

header("Access-Control-Allow-Origin: *");

require_once 'app_config.php';
require_once 'util_functions.php';
require_once 'mainFunctions.php';

####################################################################################################################
# ENTRADA: id, id_zone, name, email, id_web, id_video, id_main_image, id_rrss, id_branding
# FUNCIONAMIENTO: actualiza con los valores ingresados el stand con id en especifico
# SALIDA: devuelve informacion de error o caso contrario informa que fue actualizado correctamente
####################################################################################################################

$id = checkNull($_POST["id"]);
$id_zone = checkNull($_POST["id_zone"]);
$position = checkNull($_POST["position"]);
$name = checkNull($_POST["name"]);
$email = checkNull($_POST["email"]);
$id_web = checkNull($_POST["id_web"]);
$id_video = checkNull($_POST["id_video"]);
$id_main_image = checkNull($_POST["id_main_image"]);
$id_rrss = checkNull($_POST["id_rrss"]);
$id_branding = checkNull($_POST["id_branding"]);

# Value NULL significa no asignado, para tipo feria, galeria, teatro.

updateStand($conn, $id, $id_zone, $position, $name, $email, $id_web, $id_video, $id_main_image, $id_rrss, $id_branding);

mysqli_close($conn);


?>