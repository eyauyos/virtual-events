<?php

header("Access-Control-Allow-Origin: *");

require_once 'app_config.php';
require_once 'util_functions.php';
require_once 'mainFunctions.php';

####################################################################################################################
# ENTRADA: name
# FUNCIONAMIENTO: Crea una nueva feria e inserta en la tabla Fair
# SALIDA: devuelve informacion de error o caso contrario informa que fue insertado correctamente
####################################################################################################################

$name = checkNull($_POST["name"]);

# Value NULL significa no asignado, para tipo feria, galeria, teatro.

createFair($conn, $name);

mysqli_close($conn);


?>