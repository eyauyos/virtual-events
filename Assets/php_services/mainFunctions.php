<?php
    function createAlbumStand($conn, $id_owner, $id_image) {
        $sql = "INSERT INTO Album_stand (id_owner, id_image) VALUES ($id_owner, $id_image)";
        $result = $conn->query($sql);
        if($result) {
            echo $conn->insert_id;
        } else{ 
            echo "Error: " . $sql . "\n" . $conn->error;
        }
    }
    
    function createEvent($conn, $id_user, $name, $info, $passcode, $id_fair, $id_theater, $id_gallery) {
        $sql = "INSERT INTO Events (id_user, name, info, passcode, id_fair, id_theater, id_gallery) VALUES (
            $id_user, $name, $info, $passcode, $id_fair, $id_theater, $id_gallery)";
            
        $checkNameSql = "SELECT * FROM Events WHERE name=$name";
        $checkPasscodeSql = "SELECT * FROM Events WHERE passcode=$passcode";
         
        $resultName = $conn->query($checkNameSql);
        
        $resultPasscode = $conn->query($checkPasscodeSql);
        
        if($resultName->num_rows > 0) {
            echo "Este nombre de evento ya fue creado previamente";
        } else if($resultPasscode->num_rows > 0) {
            echo "Este passcode ya fue creado previamente";
        } else {
            $result = $conn->query($sql);
            if($result) {
                echo $conn->insert_id;
            } else{ 
                echo "Error: " . $sql . "\n" . $conn->error;
            }  
        }
    }
    
    function createFair($conn, $name) {
        $sql = "INSERT INTO Fair (name) VALUES ($name)";
        $result = $conn->query($sql);
        
        if ($result) {
          echo $conn->insert_id;
        } else {
          echo "Error: " . $sql . "\n" . $conn->error;
        }
    }

    function createGallery($conn, $name) {
        $sql = "INSERT INTO Gallery (name) VALUES ($name)";
        
        $result = $conn->query($sql);
        if ($result) {
          echo $conn->insert_id;
        } else {
          echo "Error: " . $sql . "\n" . $conn->error;
        }
    }
    
    function createModelType3d($conn, $name) {
        $sql = "INSERT INTO Model3D_types (name) VALUES ($name)";
        $result = $conn->query($sql);
        if ($result) {
            echo $conn->insert_id;
        } else {
            echo "Error: " . $sql . "\n" . $conn->error;
        }
    }
    
    function createRRSS($conn, $facebook_url, $twitter_url, $instagram_url, $linkedn_url, $whatsapp_url) {
        $sql = "INSERT INTO RRSS (facebook_url, twitter_url, instagram_url, linkedn_url, whatsapp_url) VALUES (
            $facebook_url, $twitter_url, $instagram_url, $linkedn_url, $whatsapp_url)";
        
        $result = $conn->query($sql);
        
        if ($result) {
          echo $conn->insert_id;
        } else {
          echo "Error: " . $sql . "\n" . $conn->error;
        }
    }

    function createStand($conn, $id_zone, $position, $name, $email, $id_web, $id_video, $id_main_image, $id_rrss, $id_branding) {
        $sql = "INSERT INTO Stands (id_zone, position, name, email, id_web, id_video, id_main_image, id_rrss, id_branding) VALUES (
            $id_zone, $position, $name, $email, $id_web, $id_video, $id_main_image, $id_rrss, $id_branding)";
        
        $result = $conn->query($sql);
        if ($result) {
            //echo $sql;
          echo $conn->insert_id;
        } else {
          echo "Error: " . $sql . "\n" . $conn->error;
        }
    }
    
    function createTheater($conn, $name) {
        $sql = "INSERT INTO Theater (name) VALUES (
            $name)";
        $result = $conn->query($sql);
        if ($result) {
          echo $conn->insert_id;
        } else {
          echo "Error: " . $sql . "\n" . $conn->error;
        }
    }
    
    function createVideo($conn, $url) {
        $sql = "INSERT INTO Videos (url) VALUES ($url)";
        $result = $conn->query($sql);
        if ($result) {
            echo $conn->insert_id;
        } else {
            echo "Error: " . $sql . "\n" . $conn->error;
        }
    }

    function createWeb($conn, $url) {
        $sql = "INSERT INTO Web (url) VALUES ($url)";
        
        $result = $conn->query($sql);
        
        if ($result) {
          echo $conn->insert_id;
        } else {
          echo "Error: " . $sql . "\n" . $conn->error;
        }
    }
    
    function createZoneFair($conn, $id_fair, $model_3d_type, $name) {
        $sql = "INSERT INTO Zone_fair (id_fair, model_3d_type, name) VALUES (
            $id_fair, $model_3d_type, $name)";
        
        $result = $conn->query($sql);
        
        if ($result) {
          echo $conn->insert_id;
        } else {
          echo "Error: " . $sql . "\n" . $conn->error;
        }
    }
    
    function createImage($conn, $url, $position) {
        $sql = "INSERT INTO Images (url, position) VALUES ($url, $position)";
        
        $result = $conn->query($sql);
        
        if ($result) {
          echo $conn->insert_id;
        } else {
          echo "Error: " . $sql . "\n" . $conn->error;
        }
    }
    
    function deleteImage($conn, $id_img) {
        $sql = "DELETE FROM Images WHERE id=$id_img";
        
        ##############
        $checkMainImageStandSql = "SELECT * FROM Stands WHERE id_main_image = $id_img";
        $checkBrandingImageStandSql = "SELECT * FROM Stands WHERE id_branding = $id_img";
        $checkImagePictureSql = "SELECT * FROM Pictures WHERE id_image = $id_img";
        $checkAlbumStandSql = "SELECT * FROM Album_stand WHERE id_image = $id_img";
        $checkAlbumPicturesSql = "SELECT * FROM Album_pictures WHERE id_image = $id_img";
        
        $resultMainImage = $conn->query($checkMainImageStandSql);
        $resultBrandingImage = $conn->query($checkBrandingImageStandSql);
        $resultImagePicture = $conn->query($checkImagePictureSql);
        $resultAlbumStand = $conn->query($checkAlbumStandSql);
        $resultAlbumPictures = $conn->query($checkAlbumPicturesSql);
        ###############
        
        $result = $conn->query($sql);
        if ($result === TRUE) {
            echo "Eliminado correctamente $id_img\n";
            if($resultMainImage->num_rows > 0) {
                $query1 = "UPDATE Stands SET id_main_image=0 WHERE id_main_image=$id_img";
                $conn->query($query1);
                echo "id_main_image=0 en Stand\n";
            }
            
            if($resultBrandingImage->num_rows > 0) {
                $query2 = "UPDATE Stands SET id_branding=0 WHERE id_branding=$id_img";
                $conn->query($query2);
                echo "id_branding=0 en Stand\n";
            }
            
            if($resultImagePicture->num_rows > 0) {
                $query3 = "UPDATE Pictures SET id_image=0 WHERE id_image=$id_img";
                $conn->query($query3);
                echo "id_image=0 en Pictures\n";
            }
            
            if($resultAlbumStand->num_rows > 0) {
                $query4 = "DELETE FROM Album_stand WHERE id_image=$id_img";
                $conn->query($query4);
                echo "eliminado en Album_stand";
            }
            
            if($resultAlbumPictures->num_rows > 0) {
                $query5 = "DELETE FROM Album_pictures WHERE id_image=$id_img";
                $conn->query($query5);
                echo "eliminado en Album_pictures";
            }
        }
        else {
            echo "Error: " . $sql . "\n" . $conn->error;
        }
    }

        function deleteRRSS($conn, $id_rrss) {
        $sql = "DELETE FROM RRSS WHERE id=$id_rrss";
        
        ##########
        $checkRRSS_StandSql = "SELECT * FROM Stands WHERE id_rrss=$id_rrss";
        $checkRRSS_PicturesSql = "SELECT * FROM Stands WHERE id_rrss=$id_rrss";
        
        $resultRRSS_Stand = $conn->query($checkRRSS_StandSql);
        $resultRRSS_Pictures = $conn-> query($checkRRSS_PicturesSql);
        ##########
        
        $result = $conn->query($sql);
        
        if($result===TRUE) {
            echo "RRSS eliminada correctamente $id_rrss\n";
            if($resultRRSS_Stand->num_rows > 0) {
                $query1 = "UPDATE Stands SET id_rrss=0 WHERE id_rrss=$id_rrss";
                $conn->query($query1);
                echo "id_rrss=0 en Stand\n";
            }
            if($resultRRSS_Pictures->num_rows > 0) {
                $query2 = "UPDATE Pictures SET id_rrss=0 WHERE id_rrss=$id_rrss";
                $conn->query($query2);
                echo "id_rrss=0 en Pictures\n";
            }
        }
        else {
            echo "Error: " . $sql . "\n" . $conn->error;
        }
    }
    
    function deleteVideo($conn, $id_video) {
        $sql = "DELETE FROM Videos WHERE id=$id_video";
        
        #########
        $checkVideoStandSql = "Select * FROM Stands WHERE id_video=$id_video";
        $checkVideoPicture = "Select * FROM Pictures WHERE id_video=$id_video";
        
        $resultVideoStand = $conn->query($checkVideoStandSql);
        $resultVideoPicture = $conn->query($checkVideoPicture);
        #########
        
        $result = $conn->query($sql);
        
        if($result===TRUE) {
            echo "Video eliminado correctamente $id_video\n";
            if($resultVideoStand->num_rows > 0) {
                $query1 = "UPDATE Stands SET id_video=0 WHERE id_video=$id_video";
                $conn->query($query1);
                echo "id_video=0 en Stand\n";
            }
            if($resultVideoPicture->num_rows > 0) {
                $query2 = "UPDATE Pictures SET id_video=0 WHERE id_video=$id_video";
                $conn->query($query2);
                echo "id_video=0 en Picture\n";
            }
        } else {
            echo "Error: " . $sql . "\n" . $conn->error;
        }
    }

    function deleteWeb($conn, $id_web) {
        $sql = "DELETE FROM Web WHERE id=$id_web";
        
        ########
        $checkWebStandSql = "SELECT * FROM Stands WHERE id_web=$id_web";
        $checkWebPictureSql = "SELECT * FROM Stands WHERE id_web=$id_web";
        $checkActivitiesSql = "SELECT * FROM Activities WHERE id_web=$id_web";
        
        $resultWebStand = $conn->query($checkWebStandSql);
        $resultWebPictures = $conn->query($checkWebPictureSql);
        $resultWebActivities = $conn->query($checkActivitiesSql);
        ########
        
        $result = $conn->query($sql);
        
        if($result===TRUE) {
            echo "Web eliminada correctamente $id_web\n";
            if($resultWebStand->num_rows > 0) {
                $query1 = "UPDATE Stands SET id_web=0 WHERE id_web=$id_web";
                $conn->query($query1);
                echo "id_web=0 en Stand\n";
            }
            
            if($resultWebPictures->num_rows > 0) {
                $query2 = "UPDATE Pictures SET id_web=0 WHERE id_web=$id_web";
                $conn->query($query2);
                echo "id_web=0 en Pictures\n";
            }
            
            if($resultWebActivities->num_rows > 0) {
                $query3 = "UPDATE Activities SET id_web=0 WHERE id_web=$id_web";
                $conn->query($query3);
                echo "id_web=0 en Activities\n";
            }
        }
        else {
            echo "Error: " . $sql . "\n" . $conn->error;
        }
    }
    
    function deleteModel3d($conn, $id) {
        $sql = "DELETE FROM Model3D_types WHERE id=$id";
        $result = $conn->query($sql);
        if($result===TRUE) {
            echo "Modelo 3d: $id eliminado\n";
        } else {
            echo "Error borrando modelo 3d con id -> $id\n";
        }
    }
    
    function getAlbumPhotosByIdStand($conn, $id) {
        $sql = "SELECT Images.url, Images.position  FROM Images INNER JOIN Album_stand ON Images.id = Album_stand.id_image WHERE Album_stand.id_owner = $id";
        $result = mysqli_query($conn, $sql);
        
        //echo $passcode;
        $json = array();
        
        if ($result->num_rows > 0) {
          // output data of each row
          while($row=mysqli_fetch_assoc($result)) {
              
            $json[] = $row;
            
            #echo "\n". $row["id"]."\n";
            #echo "\n". $row["id_user"]."\n";
            #echo "\n". $row["name"]."\n";
            #echo "\n". $row["info"]."\n";
            #echo "\n". $row["id_fair"]."\n";
            #echo "\n". $row["id_theater"]."\n";
            #echo "\n". $row["id_gallery"]."\n";
          }
          print(json_encode($json));
        } else {
          echo "\n0 results";
        }
    }
    
    function getEventsByPassCode($conn, $passcode) {
        $sql = "SELECT * FROM Events WHERE passcode= $passcode";
        $result = mysqli_query($conn, $sql);
        
        //echo $passcode;
        $json = array();
        
        if ($result->num_rows > 0) {
          // output data of each row
          while($row=mysqli_fetch_assoc($result)) {
              
            unset($row["id"]);
            unset($row["id_user"]);
            unset($row["passcode"]);  
            $json[] = $row;
            
            #echo "\n". $row["id"]."\n";
            #echo "\n". $row["id_user"]."\n";
            #echo "\n". $row["name"]."\n";
            #echo "\n". $row["info"]."\n";
            #echo "\n". $row["id_fair"]."\n";
            #echo "\n". $row["id_theater"]."\n";
            #echo "\n". $row["id_gallery"]."\n";
          }
          print(json_encode($json));
        } else {
          echo "\n0 results";
        }
    }
    
    function getFairById($conn, $id) {
        $sql = "SELECT * FROM Fair WHERE id= $id";
        $result = mysqli_query($conn, $sql);
        
        $json = array();
        
        if ($result->num_rows > 0) {
          // output data of each row
          while($row=mysqli_fetch_assoc($result)) {
            unset($row["id"]);
            $json[] = $row;
            #echo "\n". $row["id"]."\n";
            #echo "\n". $row["id_user"]."\n";
            #echo "\n". $row["name"]."\n";
            #echo "\n". $row["info"]."\n";
            #echo "\n". $row["id_fair"]."\n";
            #echo "\n". $row["id_theater"]."\n";
            #echo "\n". $row["id_gallery"]."\n";
          }
          print(json_encode($json));
        } else {
          echo "\n0 results";
        }
    }
    
    function getRrssById($conn, $id) {
        $sql = "SELECT * FROM RRSS WHERE id= $id";
        $result = mysqli_query($conn, $sql);
        $json = array();
        
        if ($result->num_rows > 0) {
          // output data of each row
          while($row=mysqli_fetch_assoc($result)) {
            unset($row["id"]);
            $json[] = $row;
          }
          print(json_encode($json));
        } else {
          echo "\n0 results";
        }
    }

    function getStandByIdZone($conn, $id_zone) {
        $sql = "SELECT * FROM Stands WHERE id_zone= $id_zone";
        $result = mysqli_query($conn, $sql);
        
        $json = array();
        
        if ($result->num_rows > 0) {
          // output data of each row
          while($row=mysqli_fetch_assoc($result)) {
            unset($row["id_zone"]);
            $json[] = $row;
            #echo "\n". $row["id"]."\n";
            #echo "\n". $row["id_user"]."\n";
            #echo "\n". $row["name"]."\n";
            #echo "\n". $row["info"]."\n";
            #echo "\n". $row["id_fair"]."\n";
            #echo "\n". $row["id_theater"]."\n";
            #echo "\n". $row["id_gallery"]."\n";
          }
          print(json_encode($json));
        } 
        else {
          echo "\n0 results";
        }
    }
    
    function getZoneFairByIdFair($conn, $id_fair) {
        $sql = "SELECT * FROM Zone_fair WHERE id_fair= $id_fair";
        $result = mysqli_query($conn, $sql);
        $json = array();
        if ($result->num_rows > 0) {
          // output data of each row
          while($row=mysqli_fetch_assoc($result)) {
            unset($row["id_fair"]);
            $json[] = $row;
          }
          print(json_encode($json));
        } else {
          echo "\n0 results";
        }
    }
    
    function updateEvent($conn, $id_user, $name, $info, $passcode, $id_fair, $id_theater, $id_gallery) {
        $sql = "UPDATE Events SET id_user=$id_user, name=$name, info=$info, passcode=$passcode, id_fair=$id_fair, id_theater=$id_theater, id_gallery=$id_gallery
            WHERE passcode=$passcode";
        
        if ($conn->query($sql) === TRUE) {
          echo "Evento actualizado correctamente";
        } else {
          echo "Error: " . $sql . "\n" . $conn->error;
        }
    }
    
    function updateFair($conn, $name, $newName) {
        $sql = "UPDATE Fair SET name=$newName
            WHERE name=$name";
        
        if ($conn->query($sql) === TRUE) {
          echo "Feria actualizada correctamente";
        } else {
          echo "Error: " . $sql . "\n" . $conn->error;
        }
    }
    
    function updateGallery($conn, $name,  $newName) {
        $sql = "UPDATE Gallery SET name=$newName
            WHERE name=$name";
        
        if ($conn->query($sql) === TRUE) {
          echo "Galeria actualizada correctamente";
        } else {
          echo "Error: " . $sql . "\n" . $conn->error;
        }
    }
    
    function updateModelType3d($conn, $id, $name) {
        $sql = "UPDATE Model3D_types SET name=$name WHERE id=$id";
        
        if ($conn->query($sql) === TRUE) {
          echo "Model3D actualizado correctamente";
        } else {
          echo "Error: " . $sql . "\n" . $conn->error;
        }
    }
    
    function updateRRSS($conn, $facebook_url, $twitter_url, $instagram_url, $linkedn_url, $whatsapp_url, $id) {

        $sql = "UPDATE RRSS SET facebook_url=$facebook_url, twitter_url=$twitter_url, instagram_url=$instagram_url, 
                linkedn_url=$linkedn_url, whatsapp_url=$whatsapp_url WHERE id=$id";
        
        if ($conn->query($sql) === TRUE) {
          echo "RRSS actualizado correctamente";
        } else {
          echo "Error: " . $sql . "\n" . $conn->error;
        }
    }
    
    function updateStand($conn, $id, $id_zone, $position, $name, $email, $id_web, $id_video, $id_main_image, $id_rrss, $id_branding) {
        $sql = "UPDATE Stands SET id_zone=$id_zone, position=$position, name=$name, email=$email, id_web=$id_web, id_video=$id_video, id_main_image=$id_main_image,
                id_rrss=$id_rrss, id_branding=$id_branding WHERE id=$id";
        
        if ($conn->query($sql) === TRUE) {
          echo "Stand actualizado correctamente";
        } else {
          echo "Error: " . $sql . "\n" . $conn->error;
        }      
    }
    
    function updateTheater($conn, $name, $newName) {
        $sql = "UPDATE Theater SET name=$newName
            WHERE name=$name";
        
        if ($conn->query($sql) === TRUE) {
          echo "Auditorio actualizada correctamente";
        } else {
          echo "Error: " . $sql . "\n" . $conn->error;
        }
    }
    
    function updateZoneFair($conn, $id, $id_fair, $model_3d_type, $name) {
        $sql = "UPDATE Zone_fair SET id_fair = $id_fair, model_3d_type = $model_3d_type, name = $name WHERE id=$id";
        
        if ($conn->query($sql) === TRUE) {
          echo "zona de feria actualizado correctamente";
        } else {
          echo "Error: " . $sql . "\n" . $conn->error;
        }
    }
    
    function deleteAlbumStand($conn, $id) {
        $sql = "DELETE Images FROM Images INNER JOIN Album_stand ON Images.id = Album_stand.id_image WHERE Album_stand.id_owner = $id";
        $result = $conn->query($sql);
        

        if($result===TRUE) {
            echo "Imagenes de albumStand eliminado correctamente";
            $sql1 = "DELETE FROM Album_stand WHERE id_owner=$id";
            $conn->query($sql1);
        } else {
            echo "Falla al eliminar";
        }
        
    }
    
    function deleteStand($conn, $id_stand) {
        $sql = "SELECT * FROM Stands WHERE id=$id_stand";
        $result = $conn->query($sql);
        $row=mysqli_fetch_assoc($result);
        
        
        if($result->num_rows > 0) {
            $sql = "DELETE FROM Stands WHERE id=$id_stand";
            
            $result = $conn->query($sql);
            
            if($result===TRUE) {
                echo "Stand eliminado correctamente $id_stand\n";
                deleteImage($conn, $row['id_main_image']);
                deleteImage($conn, $row['id_branding']);
                deleteRRSS($conn, $row['id_rrss']);
                deleteWeb($conn, $row['id_web']);
                deleteVideo($conn, $row['id_video']);
                deleteAlbumStand($conn, $row['id']);
            }
            else {
                echo "Error: " . $sql . "\n" . $conn->error;
            }
        } else {
            echo "Event $id_stand: 0 results";
        }
    }
    
    
    function deleteZoneFair($conn, $id_zone) {
        $sql = "SELECT * FROM Stands WHERE id_zone= $id_zone";
        $result = mysqli_query($conn, $sql);
        
        $sqlGetZone = "SELECT * FROM Zone_fair WHERE id=$id_zone";
        $resultZone = mysqli_query($conn, $sqlGetZone);
        
        if($resultZone->num_rows > 0) {
            $sqlDeleteZone = "DELETE FROM Zone_fair WHERE id= $id_zone";
            mysqli_query($conn, $sqlDeleteZone);
            if ($result->num_rows > 0) {
              while($row=mysqli_fetch_assoc($result)) {
                deleteStand($conn, $row["id"]);
              }
            } 
            else {
              echo "\n0 results";
            }
        } else {
            echo "\nZone no encontrado\n";
        }
        
    }
    
    
    function deleteFair($conn, $id) {
        $sql = "SELECT * FROM Zone_fair WHERE id_fair= $id"; 
        $result = mysqli_query($conn, $sql);
        
        $sqlGetFair = "SELECT * FROM Fair WHERE id=$id";
        $resultFair = mysqli_query($conn, $sqlGetFair);
        
        if($resultFair->num_rows > 0) {
            $sqlDeleteFair = "DELETE FROM Fair WHERE id=$id";
            mysqli_query($conn, $sqlDeleteFair);
            
            if ($result->num_rows > 0) {
                while($row=mysqli_fetch_assoc($result)) {
                    deleteZoneFair($conn, $row["id"]);
                }
            } else {
              echo "\n0 results";
            }
        } else {
            echo "\nFeria no encontrado\n";
        }
        
    }
    
    function deleteAlbumPictures($conn, $id) {
        $sql = "DELETE Images FROM Images INNER JOIN Album_pictures ON Images.id = Album_pictures.id_image WHERE Album_pictures.id_owner = $id";
        $result = $conn->query($sql);
        

        if($result===TRUE) {
            echo "Imagenes de albumPictures eliminado correctamente";
            $sql1 = "DELETE FROM Album_pictures WHERE id_owner=$id";
            $conn->query($sql1);
        } else {
            echo "Falla al eliminar";
        }
    }
    
    function deletePicture($conn, $id_picture) {
        $sql = "SELECT * FROM Pictures WHERE id=$id_picture";
        $result = $conn->query($sql);
        $row=mysqli_fetch_assoc($result);
        
        
        if($result->num_rows > 0) {
            $sql = "DELETE FROM Pictures WHERE id=$id_picture";
            
            $result = $conn->query($sql);
            
            if($result===TRUE) {
                echo "Picture eliminado correctamente $id_picture\n";
                deleteImage($conn, $row['id_image']);
                deleteRRSS($conn, $row['id_rrss']);
                deleteWeb($conn, $row['id_web']);
                deleteVideo($conn, $row['id_video']);
                deleteAlbumPictures($conn, $row['id']);
            }
            else {
                echo "Error: " . $sql . "\n" . $conn->error;
            }   
        } else {
            echo "Picture $id_picture 0 results";
        }
        
    }
    
    function deleteZoneGallery($conn, $id_zone) {
        $sql = "SELECT * FROM Pictures WHERE id_zone= $id_zone";
        $result = mysqli_query($conn, $sql);
        
        $sqlGetZone = "SELECT * FROM Zone_gallery WHERE id=$id_zone";
        $resultZone = mysqli_query($conn, $sqlGetZone);
        
        if($resultZone->num_rows > 0) {
            $sqlDeleteZone = "DELETE FROM Zone_gallery WHERE id= $id_zone";
            mysqli_query($conn, $sqlDeleteZone);
            if ($result->num_rows > 0) {
              while($row=mysqli_fetch_assoc($result)) {
                deletePicture($conn, $row["id"]);
              }
            } 
            else {
              echo "\n0 results in Pictures";
            }
        } else {
            echo "\nZone $id_zone no encontrado\n";
        }
    }
    
    function deleteGallery($conn, $id) {
        $sql = "SELECT * FROM Zone_gallery WHERE id_gallery= $id"; 
        $result = mysqli_query($conn, $sql);
        
        $sqlGetGallery = "SELECT * FROM Gallery WHERE id=$id";
        $resultGallery = mysqli_query($conn, $sqlGetGallery);
        
        if($resultGallery->num_rows > 0) {
            $sqlDeleteGallery = "DELETE FROM Gallery WHERE id=$id";
            mysqli_query($conn, $sqlDeleteGallery);
            
            if ($result->num_rows > 0) {
                while($row=mysqli_fetch_assoc($result)) {
                    deleteZoneGallery($conn, $row["id"]);
                }
            } else {
              echo "\n0 results";
            }
        } else {
            echo "\nGaleria no encontrado\n";
        }
    }
    
    
    // Definir antes funciones de teatro (pendiente)
    function deleteEvent($conn, $id) {
        $sqlGetEvent = "SELECT * FROM Events WHERE id=$id";
        $resultEvent = mysqli_query($conn, $sqlGetEvent);
        
        if($resultEvent->num_rows > 0) {
            $sqlDeleteEvent = "DELETE FROM Events WHERE id=$id";
            mysqli_query($conn, $sqlDeleteEvent);
            
            while($row=mysqli_fetch_assoc($resultEvent)) {
                deleteFair($conn, $row["id_fair"]);
                deleteGallery($conn, $row["id_gallery"]);
                //deleteTheater($conn, $row["id_theater"]);   (implementacion pendiente)
            }
        } 
    }
?> 