<?php

header("Access-Control-Allow-Origin: *");

require_once 'app_config.php';
require_once 'util_functions.php';
require_once 'mainFunctions.php';

$id = checkNull($_POST["id"]);

####################################################################################################################
# ENTRADA: name
# FUNCIONAMIENTO: hace una consulta a la base de datos y devuelve la feria con ese id
# SALIDA: devuelve la feria en forma de JSON
####################################################################################################################

getFairById($conn, $id);

mysqli_close($conn);


?>