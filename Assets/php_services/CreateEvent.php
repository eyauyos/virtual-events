<?php

header("Access-Control-Allow-Origin: *");

require_once 'app_config.php';
require_once 'util_functions.php';
require_once 'mainFunctions.php';
####################################################################################################################
# ENTRADA: id_user, name, info, passcode, id_fair, id_theater, id_gallery
# FUNCIONAMIENTO: toma los valores de entrada y los inserta a la tabla Events
# SALIDA: devuelve informaciond de error o caso contrario informa que fue insertado correctamente
####################################################################################################################

$id_user = checkNull($_POST["id_user"]);
$name = checkNull($_POST["name"]);
$info = checkNull($_POST["info"]);
$passcode = checkNull($_POST["passcode"]);
$id_fair = checkNull($_POST["id_fair"]);
$id_theater = checkNull($_POST["id_theater"]);
$id_gallery = checkNull($_POST["id_gallery"]);

createEvent($conn, $id_user, $name, $info, $passcode, $id_fair, $id_theater, $id_gallery);

mysqli_close($conn);

?>