<?php

header("Access-Control-Allow-Origin: *");

require_once 'app_config.php';
require_once 'util_functions.php';
require_once 'mainFunctions.php';

####################################################################################################################
# ENTRADA: id
# FUNCIONAMIENTO: Elimina una rrss y setea a 0 los id de esa imagen contenidas en otras tablas.
# SALIDA: devuelve informacion de error o caso contrario informa que fue borrado correctamente
####################################################################################################################

$id_rrss = checkNull($_POST["id"]);

deleteRRSS($conn, $id_rrss);

mysqli_close($conn);

?>