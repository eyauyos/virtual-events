<?php

header("Access-Control-Allow-Origin: *");

require_once 'app_config.php';
require_once 'util_functions.php';
require_once 'mainFunctions.php';

$id_fair = checkNull($_POST["id_fair"]);

####################################################################################################################
# ENTRADA: id_fair
# FUNCIONAMIENTO: hace una consulta a la base de datos y devuelve la zona de feria con ese id_fair
# SALIDA: devuelve la zona feria en forma de JSON
####################################################################################################################

getZoneFairByIdFair($conn, $id_fair);

mysqli_close($conn);


?>