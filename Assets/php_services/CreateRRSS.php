<?php

header("Access-Control-Allow-Origin: *");

require_once 'app_config.php';
require_once 'util_functions.php';
require_once 'mainFunctions.php';

####################################################################################################################
# ENTRADA: facebook_url, twitter_url, instagram_url, linkedn_urlm whatsapp_url
# FUNCIONAMIENTO: Crea una nueva zona de feria con el id_fair ingresado
# SALIDA: devuelve informacion de error o caso contrario informa que fue insertado correctamente
####################################################################################################################

$facebook_url = checkNull($_POST["facebook_url"]);
$twitter_url = checkNull($_POST["twitter_url"]);
$instagram_url = checkNull($_POST["instagram_url"]);
$linkedn_url = checkNull($_POST["linkedn_url"]);
$whatsapp_url = checkNull($_POST["whatsapp_url"]);

# Value NULL significa no asignado, para id_fair, model_3d_type, name.

createRRSS($conn, $facebook_url, $twitter_url, $instagram_url, $linkedn_url, $whatsapp_url);

mysqli_close($conn);

?>