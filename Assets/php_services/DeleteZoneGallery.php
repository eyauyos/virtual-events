<?php

header("Access-Control-Allow-Origin: *");

require_once 'app_config.php';
require_once 'util_functions.php';
require_once 'mainFunctions.php';

####################################################################################################################
# ENTRADA: id
# FUNCIONAMIENTO: Elimina un zone_gallery y elimina sus dependencias.
# SALIDA: devuelve informacion de error o caso contrario informa que fue borrado correctamente
####################################################################################################################

$id_zone = checkNull($_POST["id"]);

deleteZoneGallery($conn, $id_zone);

mysqli_close($conn);

?>