<?php

header("Access-Control-Allow-Origin: *");

require_once 'app_config.php';
require_once 'util_functions.php';
require_once 'mainFunctions.php';

####################################################################################################################
# ENTRADA: name
# FUNCIONAMIENTO: Crea un nuevo tipo de modelo 3d con nombre name
# SALIDA: devuelve informacion de error o caso contrario informa que fue insertado correctamente
####################################################################################################################

$name = checkNull($_POST["name"]);

createModelType3d($conn, $name);

mysqli_close($conn);

?>