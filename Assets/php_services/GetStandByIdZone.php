<?php

header("Access-Control-Allow-Origin: *");

require_once 'app_config.php';
require_once 'util_functions.php';
require_once 'mainFunctions.php';

$id_zone = checkNull($_POST["id_zone"]);

####################################################################################################################
# ENTRADA: id_zone
# FUNCIONAMIENTO: hace una consulta a la base de datos y devuelve los stands con ese id_zone
# SALIDA: devuelve los stands en forma de JSON
####################################################################################################################

getStandByIdZone($conn, $id_zone);

mysqli_close($conn);


?>