<?php

header("Access-Control-Allow-Origin: *");

require_once 'app_config.php';
require_once 'util_functions.php';
require_once 'mainFunctions.php';

####################################################################################################################
# ENTRADA: url
# FUNCIONAMIENTO: Crea un nuevo video en la tabla Web con el url ingresado
# SALIDA: devuelve informacion de error o caso contrario informa que fue insertado correctamente
####################################################################################################################

$url = checkNull($_POST["url"]);

# Value NULL significa no asignado

createWeb($conn, $url);

mysqli_close($conn);

?>