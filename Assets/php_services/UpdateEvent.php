<?php

header("Access-Control-Allow-Origin: *");

require_once 'app_config.php';
require_once 'util_functions.php';
require_once 'mainFunctions.php';

####################################################################################################################
# ENTRADA: id_user, name, info, passcode, id_fair, id_theater, id_gallery
# FUNCIONAMIENTO: actualiza con los valores ingresados al evento con el passcode en especifico
# SALIDA: devuelve informacion de error o caso contrario informa que fue actualizado correctamente
####################################################################################################################

$id_user = checkNull($_POST["id_user"]);
$name = checkNull($_POST["name"]);
$info = checkNull($_POST["info"]);
$passcode = checkNull($_POST["passcode"]);
$id_fair = checkNull($_POST["id_fair"]);
$id_theater = checkNull($_POST["id_theater"]);
$id_gallery = checkNull($_POST["id_gallery"]);

# Value NULL significa no asignado, para tipo feria, galeria, teatro.

updateEvent($conn, $id_user, $name, $info, $passcode, $id_fair, $id_theater, $id_gallery);

mysqli_close($conn);


?>