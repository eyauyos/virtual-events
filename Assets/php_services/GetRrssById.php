<?php

header("Access-Control-Allow-Origin: *");

require_once 'app_config.php';
require_once 'util_functions.php';
require_once 'mainFunctions.php';

$id = checkNull($_POST["id"]);

####################################################################################################################
# ENTRADA: id
# FUNCIONAMIENTO: hace una consulta a la base de datos y devuelve la rrss con ese nombre
# SALIDA: devuelve la rrss en forma de JSON
####################################################################################################################

getRrssById($conn, $id);

mysqli_close($conn);


?>