<?php

header("Access-Control-Allow-Origin: *");

require_once 'app_config.php';
require_once 'util_functions.php';
require_once 'mainFunctions.php';

$passcode = checkNull($_POST["passcode"]);

####################################################################################################################
# ENTRADA: passcode
# FUNCIONAMIENTO: hace una consulta a la base de datos y devuelve el evento con ese passcode
# SALIDA: devuelve el evento en forma de JSON
####################################################################################################################

getEventsByPassCode($conn, $passcode);

mysqli_close($conn);


?>