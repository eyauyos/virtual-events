<?php

header("Access-Control-Allow-Origin: *");

require_once 'app_config.php';
require_once 'util_functions.php';
require_once 'mainFunctions.php';

####################################################################################################################
# ENTRADA: url, position
# FUNCIONAMIENTO: Crea un nuevo video en la tabla  Images con el url, position ingresado
# SALIDA: devuelve informacion de error o caso contrario informa que fue insertado correctamente
####################################################################################################################

$url = checkNull($_POST["url"]);
$position = checkNull($_POST["position"]);
# Value NULL significa no asignado

createImage($conn, $url, $position);

mysqli_close($conn);

?>