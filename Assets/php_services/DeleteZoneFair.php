<?php

header("Access-Control-Allow-Origin: *");

require_once 'app_config.php';
require_once 'util_functions.php';
require_once 'mainFunctions.php';

####################################################################################################################
# ENTRADA: id
# FUNCIONAMIENTO: Elimina un ZoneFair y elimina sus dependencias.
# SALIDA: devuelve informacion de error o caso contrario informa que fue borrado correctamente
####################################################################################################################

$id = checkNull($_POST["id"]);

deleteZoneFair($conn, $id);

mysqli_close($conn);

?>