<?php

header("Access-Control-Allow-Origin: *");

require_once 'app_config.php';
require_once 'util_functions.php';
require_once 'mainFunctions.php';

####################################################################################################################
# ENTRADA: id, name
# FUNCIONAMIENTO: actualiza con los valores ingresados al model3D con el id en especifico
# SALIDA: devuelve informacion de error o caso contrario informa que fue actualizado correctamente
####################################################################################################################

$id = checkNull($_POST["id"]);
$name = checkNull($_POST["name"]);

# Value NULL significa no asignado, para tipo feria, galeria, teatro.

updateModelType3d($conn, $id, $name);

mysqli_close($conn);


?>